import os
import requests as r
import json
import boto3
from io import BytesIO

# считываем переменные окружения
bot_token = os.environ['bot_token']
key_id = os.environ['aws_access_key_id']
secret_key = os.environ['aws_secret_access_key']
bucket_id = os.environ['bucket_id']

# указываем chat_id
chat_id = "647630536"
telegram_url = "https://api.telegram.org/bot{token}/".format(token=bot_token)

# ссылка для вызова облачной функции
server_url = "https://functions.yandexcloud.net/d4ei0qvcuo8v68nnd7ec"
# название папки, в которой будут сохраняться папки с именами людей и их фото
people_folder_prefix = 'people/'

session = boto3.Session(
    aws_access_key_id=key_id,
    aws_secret_access_key=secret_key,
)
client = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
)


def handler(event, context):
    body = json.loads(event['body'])
    if 'message' in body:
        message = body['message']

        # срабатывает, если был отправлен ответ на сообщение (т.е. отвечанное)
        if 'reply_to_message' in message:
            reply_msg = message['reply_to_message']
            if 'photo' in reply_msg and 'caption' in reply_msg:
                text = reply_msg['caption']
                object_id = text.split('|')[0]
                name = message['text']
                upload_photo(object_id, name.lower())

        # срабатывает, если была отправлена команда /find
        if message['text'].startswith('/find'):
            name = message['text'][6:].lower()
            send_person_photos(name)
    return {
        'statusCode': 200,
    }


def set_webhook():
    r.post(url=telegram_url + 'setWebhook', data={'url': server_url})


set_webhook()


# функция для отправки текстового сообщения в Telegram Bot
def send_message(text):
    params = {
        'chat_id': chat_id,
        'text': text
    }
    return r.post(url=telegram_url + 'sendMessage', data=params)


# функция для отправки фотографии в Telegram Bot
def send_photo(object_id):
    f = get_object_file(object_id)
    return r.post(url=telegram_url + 'sendPhoto', params={'chat_id': chat_id}, files={'photo': f})


def get_file(object_id):
    file = bytearray()
    file[:] = get_object_file(object_id)
    return BytesIO(file)


# функция для получения файла из бакета
def get_object_file(object_id):
    return client.get_object(Bucket=bucket_id, Key=object_id).get('Body').read()


# функция для загрузки файлов в бакет
def upload_photo(object_id, name):
    photo_id = people_folder_prefix + name + '/' + object_id
    client.upload_fileobj(get_file(object_id), bucket_id, photo_id)


# функция для получения и отправки оригинальных фотографий, где был отмечен человек
def send_person_photos(name):
    list_objects = client.list_objects(Bucket=bucket_id, Prefix=people_folder_prefix + name)
    # проверяем, есть ли объекты в папке с таким именем
    if 'Contents' not in list_objects:
        send_message('Фотографии с ' + name + ' не найдены')
    else:
        photos = set()
        for obj in list_objects['Contents']:
            paths = obj['Key'].split('/')
            key = paths[len(paths) - 2]
            photos.add(key)
        # проходимя по всем фотографиям, где был человек с таким именем
        for photo in photos:
            send_photo(photo)