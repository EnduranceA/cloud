import requests as r
import json
import boto3
import os

# получаем переменные окружения
bot_token = os.environ['bot_token']
key_id = os.environ['aws_access_key_id']
secret_key = os.environ['aws_secret_access_key']
bucket_id = os.environ['bucket_id']

# указываем chat_id
chat_id = "647630536"
telegram_url = "https://api.telegram.org/bot{token}/".format(token=bot_token)

session = boto3.Session(
    aws_access_key_id=key_id,
    aws_secret_access_key=secret_key,
)
client = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
)

def handler(event, context):
    for message in event['messages']:
        body = json.loads(message['details']['message']['body'])
        if body['type'] == 'keys':
            for face_id in body['data']:
                send_photo(face_id)
    return {
        'statusCode': 200,
    }

# функция для получения файла из бакета
def get_object_file(object_id):
    return client.get_object(Bucket=bucket_id, Key=object_id).get('Body').read()

# функция для отправки фото в Telegram Bot
def send_photo(object_id):
    f = get_object_file(object_id)
    params = {
        'chat_id': chat_id,
        'caption': object_id + '|\nКто это?'}
    return r.post(url=telegram_url + 'sendPhoto', params=params, files= {'photo': f})