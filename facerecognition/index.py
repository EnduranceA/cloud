import requests
import json
from PIL import Image
from io import BytesIO
import boto3
import os
import base64

# функция, которая кодирует файл и возвращает результат.
def encode_file(content):
  base64_bytes = base64.b64encode(content)
  return base64_bytes.decode('ascii') 

def get_coordinates(coord):
    left = int(coord[0].get('x'))
    top = int(coord[0].get('y'))
    right = int(coord[2].get('x'))
    bottom = int(coord[2].get('y'))
    return (left, top, right, bottom)

def get_faces(token, message, enfile):
    url_vision = 'https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze'
    headers = {
        'Authorization': 'Bearer ' + token
    }
    return requests.post(url_vision, headers=headers, json =
        {            
            "folderId": message.get('event_metadata')['folder_id'],
            "analyze_specs": [{
            "content": enfile,
            "features": [{
                    "type": "FACE_DETECTION"
                    }]
                            }]
        }).json()['results']

def send_message(keys, client_for_queue):
    client_for_queue.send_message(
        QueueUrl='https://message-queue.api.cloud.yandex.net/b1gs4a51unfsngpt0hke/dj6000000003petk06dt/crop_images',
        MessageBody=json.dumps({
            'type' : 'keys',
            'data' : json.dumps(keys)
        })
    )

def handler(event, context):
    message = event['messages'][0]
    # информация о загруженной фотографии
    details = message.get('details')
    #проверяем, что на вход поступила оригинальная фотография
    if '/' not in details['object_id']:
        #считываем переменные окружения
        key_id = os.environ['aws_access_key_id']
        secret_key = os.environ['aws_secret_access_key']

        session = boto3.Session(
            aws_access_key_id=key_id,
            aws_secret_access_key=secret_key,
        )
        client = session.client(
            service_name='s3',
            endpoint_url='https://storage.yandexcloud.net',
        )
        client_for_queue = session.client(
            service_name='sqs',
            endpoint_url='https://message-queue.api.cloud.yandex.net',
            region_name='ru-central1'
        )
        # получаем файл
        f = client.get_object(Bucket=details['bucket_id'], Key=details['object_id']).get('Body')
        # считываем его содержимое
        content = f.read()
        results = get_faces(context.token.get('access_token'), message, encode_file(content));
        
        res = results[0]['results'][0]['faceDetection']
        if bool(res):
            faces = res['faces']
            keys = []
            for j in range(len(faces)):
                coord = faces[j]['boundingBox']['vertices']
                img = Image.open(BytesIO(content))
                
                img_crop = img.crop(get_coordinates(coord))

                buf = BytesIO()
                img_crop.save(buf, format='JPEG')
                key_image = details['object_id'] + '/' + 'crop' + str(j) + '.jpg'
                client.upload_fileobj(BytesIO(buf.getvalue()), details['bucket_id'], key_image)
                keys.append(key_image)
            
            if len(keys) > 0:
                send_message(keys, client_for_queue);
        return {
            'statusCode': 200,
        }

